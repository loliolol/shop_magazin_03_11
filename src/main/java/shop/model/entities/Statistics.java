package shop.model.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "statistics")
@NoArgsConstructor
public class Statistics {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    @Column(name = "date")
    private Date date;
    @Column(name = "count")
    private int count;
    public Statistics(Product product,
                      Date date,
                      int count) {
        this.product = product;
        this.date = date;
        this.count = count;
    }
    }
