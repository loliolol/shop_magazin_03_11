package shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import shop.controllers.dto.CategoryDto;
import shop.controllers.dto.ProductDto;
import shop.model.entities.Category;
import shop.model.entities.Product;
import shop.model.repositories.CategoriesRepository;
import shop.model.repositories.ProductsRepository;

@Controller
public class AdminController {
    private final CategoriesRepository categoriesRepository;
    private final ProductsRepository productsRepository;
    @Autowired
    public AdminController (CategoriesRepository categoriesRepository,
                            ProductsRepository productsRepository) {
        this.categoriesRepository=categoriesRepository;
        this.productsRepository=productsRepository;
    }
    @GetMapping("/admin/category")
    public String categoryManagment(Model model) {
        final CategoryDto category = new CategoryDto();
        model.addAttribute("category",category);
        return "adminCategory";
    }




    @GetMapping("/admin/")
    public String admin(Model model) {
        return "admin";
    }

    @PostMapping("/admin/category")
    public String addCategory(@ModelAttribute("category") CategoryDto
                                      categoryDto) {
        final String parentName=categoryDto.getParentName();
        Category parentCategory = null;
        if (parentName != null && !parentName.isEmpty()) {
            parentCategory=categoriesRepository.findByName(parentName);
        }
        final Category newCategory = new
                Category(categoryDto.getName(), parentCategory);
        categoriesRepository.save(newCategory);
        return "redirect:/admin/category";
    }

    @GetMapping("/admin/product")
    public String productManagment(Model model) {
        final ProductDto product = new ProductDto();
        model.addAttribute("product",product);
        return "adminProduct";
    }
    @PostMapping("/admin/product")
    public String addProduct(@ModelAttribute("product") ProductDto
                                     productDto) {
        final Category
                category=categoriesRepository.findByName(productDto
                .getCategoryName());

        final Product newProduct = new Product(
                productDto.getImageUrl(),
                productDto.getName(),
                productDto.getDescription(),
                productDto.getPrice(),
                category);
        productsRepository.save(newProduct);
        return "redirect:/admin/product";
    }


    @GetMapping("/admin/category/delete")
    public String categoryDeleteManagment(Model model) {
        final CategoryDto category = new CategoryDto();
        model.addAttribute("category",category);
        return "adminCategoryDelete";
    }
    @PostMapping("/admin/category/delete")
    public String deleteCategory(@ModelAttribute("category") CategoryDto
                                      categoryDto) {
        final String categoryName=categoryDto.getName();
        final Category category = categoriesRepository.findByName(categoryDto.getName());
        if (category == null){
            return "redirect:/admin/category/delete";
        }
        categoriesRepository.delete(category);
        return "redirect:/admin/category/delete";
    }

    @GetMapping("/admin/product/delete")
    public String productDeleteManagment(Model model) {
        final ProductDto product = new ProductDto();
        model.addAttribute("product",product);
        return "adminProductDelete";
    }
    @PostMapping("/admin/product/delete")
    public String deleteProduct(@ModelAttribute("product") ProductDto
                                     productDto) {

        final String productDtoName=productDto.getName();
        final Product product = productsRepository.findByName(productDtoName);
        if (product == null){
            return "redirect:/admin/product/delete";
        }
        productsRepository.delete(product);
        return "redirect:/admin/product/delete";
    }
}

