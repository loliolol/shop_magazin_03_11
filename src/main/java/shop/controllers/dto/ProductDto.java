package shop.controllers.dto;
import lombok.Data;
import java.math.BigDecimal;
@Data
public class ProductDto {
    private String imageUrl;
    private String name;
    private String description;
    private BigDecimal price;
    private String categoryName;
}
